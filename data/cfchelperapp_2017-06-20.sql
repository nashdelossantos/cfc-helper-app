# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.6.26)
# Database: cfchelperapp
# Generation Time: 2017-06-20 06:23:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table chapter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chapter`;

CREATE TABLE `chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `chapter` WRITE;
/*!40000 ALTER TABLE `chapter` DISABLE KEYS */;

INSERT INTO `chapter` (`id`, `name`)
VALUES
	(1,'UK South');

/*!40000 ALTER TABLE `chapter` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `slug`, `initial`, `role`, `permissions`)
VALUES
	(1,'Top Admin','top_admin','TA','ROLE_TOP_ADMIN','user_can_create, user_can_read, user_can_update, user_can_delete, setting_can_create, setting_can_read, setting_can_update, setting_can_delete, export_can_create'),
	(2,'Member','member','M','ROLE_MEMBER',NULL),
	(3,'KFC Coordinator','kfc_coordinator','KFCC','ROLE_KFC_COORDINATOR',NULL),
	(4,'YFC Coordinator','yfc_coordinator','YFCC','ROLE_YFC_COORDINATOR',NULL),
	(5,'Music Ministry','music_ministry','MMC','ROLE_MUSIC_MINISTRY',NULL),
	(6,'Household Head','household_head','HH','ROLE_HOUSEHOLD_HEAD',NULL),
	(7,'Household Leader','household_leader','HL','ROLE_HOUSEHOLD_LEADER',NULL),
	(8,'Unit Head','unit_head','UH','ROLE_UNIT_HEAD',NULL),
	(9,'Unit Leader','unit_leader','UL','ROLE_UNIT_LEADER',NULL),
	(10,'Chapter Head','chapter_head','CH','ROLE_CHAPTER_HEAD',NULL),
	(11,'Chapter Leader','chapter_leader','CL','ROLE_CHAPTER_LEADER',NULL),
	(12,'Region Head','region_head','RH','ROLE_REGION_HEAD',NULL),
	(13,'Region Leader','region_leader','RL','ROLE_REGION_LEADER',NULL),
	(14,'Handmaid Coordinator','handmaid_coordinator','HC','ROLE_HANDMAID_COORDINATOR',NULL),
	(16,'Event Coordinator','event_coordinator','EC','ROLE_EVENT_COORDINATOR',NULL);

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table household
# ------------------------------------------------------------

DROP TABLE IF EXISTS `household`;

CREATE TABLE `household` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `household` WRITE;
/*!40000 ALTER TABLE `household` DISABLE KEYS */;

INSERT INTO `household` (`id`, `name`)
VALUES
	(1,'Cardiff Household'),
	(2,'Merthyr Tydfil Household'),
	(3,'Swansea Household');

/*!40000 ALTER TABLE `household` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ministry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ministry`;

CREATE TABLE `ministry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initial` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `ministry` WRITE;
/*!40000 ALTER TABLE `ministry` DISABLE KEYS */;

INSERT INTO `ministry` (`id`, `initial`, `name`)
VALUES
	(1,'Cfc','Couples For Christ'),
	(2,'Sold','Servants Of The Lord'),
	(3,'Hold','Handmaids Of The Lord'),
	(4,'Sfc','Singles For Christ'),
	(5,'Yfc','Youth For Christ'),
	(6,'Kfc','Kids For Christ'),
	(7,'Mn','Music Ministry');

/*!40000 ALTER TABLE `ministry` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pastoral
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pastoral`;

CREATE TABLE `pastoral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `venue` longtext COLLATE utf8_unicode_ci,
  `date` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_69851C36A21214B7` (`categories_id`),
  CONSTRAINT `FK_69851C36A21214B7` FOREIGN KEY (`categories_id`) REFERENCES `taxonomy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table taxonomy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taxonomy`;

CREATE TABLE `taxonomy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unit`;

CREATE TABLE `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;

INSERT INTO `unit` (`id`, `name`)
VALUES
	(1,'Unit 1'),
	(2,'Unit 2'),
	(3,'Unit 3'),
	(4,'Unit 4'),
	(5,'Unit 5');

/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `clp_date` datetime DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_tithing` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `ministry` int(11) DEFAULT NULL,
  `telephone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wedding_date` datetime DEFAULT NULL,
  `leader` int(11) DEFAULT NULL,
  `chapter` int(11) DEFAULT NULL,
  `household` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `nickname` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  KEY `IDX_8D93D649889C0531` (`ministry`),
  KEY `IDX_8D93D649F5E3EAD7` (`leader`),
  KEY `IDX_8D93D649F981B52E` (`chapter`),
  KEY `IDX_8D93D64954C32FC0` (`household`),
  KEY `IDX_8D93D649DCBB0C53` (`unit`),
  CONSTRAINT `FK_8D93D64954C32FC0` FOREIGN KEY (`household`) REFERENCES `household` (`id`),
  CONSTRAINT `FK_8D93D649889C0531` FOREIGN KEY (`ministry`) REFERENCES `ministry` (`id`),
  CONSTRAINT `FK_8D93D649DCBB0C53` FOREIGN KEY (`unit`) REFERENCES `unit` (`id`),
  CONSTRAINT `FK_8D93D649F5E3EAD7` FOREIGN KEY (`leader`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_8D93D649F981B52E` FOREIGN KEY (`chapter`) REFERENCES `chapter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `email`, `password`, `firstname`, `lastname`, `birthday`, `clp_date`, `address1`, `address2`, `city`, `postcode`, `country`, `avatar`, `is_tithing`, `is_active`, `ministry`, `telephone`, `mobile`, `wedding_date`, `leader`, `chapter`, `household`, `unit`, `nickname`)
VALUES
	(1,'nashsaint@gmail.com','$2y$13$BLfCAWVXk1gQWSCluRZr0u0Qt95salyNMcepUc/Uos8fhiuYc92xW','Nash','Delos Santos','1976-06-12 00:00:00','2013-09-21 00:00:00','40 Roundwood','Llanedeyr','Cardiff','CF23 9PF','United Kingdom',NULL,0,1,1,NULL,'07894790939','2014-09-21 00:00:00',1,NULL,NULL,NULL,NULL),
	(7,'sean_haran@hotmail.com','$2y$13$lDvZfr3fMwmSXRR.mb5L9./qtqt9ngIdOxHZ3xA6kKoJDk/RGKrB6','Sean','Haran','2017-06-01 00:00:00','2004-04-03 00:00:00','11 Carlton House','Clydach','Swansea','SA6 6JN','United Kingdom',NULL,0,1,1,'01792 849424',NULL,'2010-09-18 00:00:00',1,NULL,NULL,NULL,NULL),
	(8,'mdbercero@yahoo.com','$2y$13$aJIfGacoVROKXB.hSQ1IOuhLFtBqPfdj.iH239E78CGSymvDI4oZG','Maria Veliza','Haran','2017-01-25 00:00:00','2004-04-03 00:00:00','11 Carlton House','Clydach','Swansea','SA6 6JN','United Kingdom',NULL,0,1,1,NULL,NULL,'2010-09-18 00:00:00',1,NULL,NULL,NULL,NULL),
	(12,'rgsibayan@yahoo.com','$2y$13$Z2eJxqt5x0k.CnncNUTbW.9JEObvfAeRP9IvSx8hu2TnQwixQwJUC','Roel Benigno','Sibayan','1969-02-13 00:00:00','2005-04-02 00:00:00','31 Heol y Deri','Morriston','Swansea','SA6 6JH','United Kingdom',NULL,0,1,1,'01972 797709',NULL,'1997-12-20 00:00:00',1,NULL,NULL,NULL,NULL),
	(13,'roy_813@yahoo.com','$2y$13$SuF8WhcmvGmAhVjlAJnSbu6dGNBV3JBn8bhjVAtEQ7MDYxkoZdqAC','Roy','Buendia','1971-08-13 00:00:00','2006-03-04 00:00:00','14 Rhodfar Eos','Morriston','Swansea','SA6 6TF','United Kingdom',NULL,0,1,1,'01792 517372',NULL,'2010-04-03 00:00:00',1,NULL,NULL,NULL,NULL),
	(14,'mygs_uk@yahoo.co.uk','$2y$13$Xf2Au5yhWiBuhnjD8w0jtuQqj/CJEPjIUIMKHls9YWS.wJRxYante','Mygnon','Buendia','2017-03-08 00:00:00','2006-03-11 00:00:00','14 Rhodfar Eos','Morriston','Swansea','SA6 6TF','United Kingdom',NULL,0,1,1,NULL,NULL,'2010-04-03 00:00:00',1,NULL,NULL,NULL,NULL),
	(15,'chi2fab@yahoo.com','$2y$13$ym7eSbXEledJiWERy4b.n.vQSQLnZbGMDpTwRWLPnEfbxx0q3MGE6','Chito','Fababeir','1971-04-21 00:00:00','2006-08-01 00:00:00','7 Clos Ebol','Morriston','Swansea','SA6 6TP','United Kingdom',NULL,0,1,1,NULL,NULL,'2000-03-04 00:00:00',1,NULL,NULL,NULL,NULL),
	(16,'grace2fab@yahoo.co.uk','$2y$13$gRwEaSTvHSkbCRmYbc1tY.2lHNypSTYeX5Nb0yDSgnDTEa8by3mdC','Natalie Grace','Fababeir','1971-11-30 00:00:00','2010-04-03 00:00:00','7 Clos Ebol','Morriston','Swansea','SA6 6TP','United Kingdom',NULL,0,1,1,'01792 774858',NULL,'2000-03-04 00:00:00',1,NULL,NULL,NULL,NULL),
	(17,'jdtumbali2001@yahoo.com','$2y$13$KXplhsXAzmWkEMAFMrJ9kuc1tpKjiYz2xuW08Ma/9p01yZaWD66FG','James','Tumbali','1966-05-04 00:00:00','2016-05-06 00:00:00','122 Heol Maes y Gelenen','Morriston','Swansea','SA6 6JT','United Kingdom',NULL,0,0,1,NULL,NULL,'1991-07-28 00:00:00',1,NULL,NULL,NULL,NULL),
	(18,'isitum1264@yahoo.com','$2y$13$DdHFwTQoTTp3iQuM6I5Hj.N4WegngtJl8JyFEEsdXgQPGRmwBbBg2','Isidra','Tumbali','1964-12-28 00:00:00','2006-05-06 00:00:00','122 Heol Maes y Gelenen,','Morriston','Swansea','SA6 6JT','United Kingdom',NULL,0,0,1,NULL,NULL,'2017-07-28 00:00:00',1,NULL,NULL,NULL,NULL),
	(19,NULL,'$2y$13$RqUGuOqOwBJMNdqGwQCtAu6vyPYO24dlfue.i2oawMQuvtc0Vul6S','Louie','Dominguiano','1976-10-08 00:00:00','2008-04-05 00:00:00',NULL,'Morriston','Swansea',NULL,'United Kingdom',NULL,0,1,1,NULL,NULL,'2001-05-01 00:00:00',1,NULL,NULL,NULL,NULL),
	(20,'evamariedominguiano@yahoo.com','$2y$13$sq5Zcg1J0VvYeEjJtg3w6Ofbxuk1Hg6FYgs4wRCR1fiswuDt90a66','Eva Marie','Dominguiano','1978-01-04 00:00:00','2010-08-13 00:00:00',NULL,'Morriston','Swansea',NULL,'United Kingdom',NULL,0,1,1,NULL,NULL,'2000-03-04 00:00:00',1,NULL,NULL,NULL,NULL),
	(21,'fdelacruz42@yahoo.com','$2y$13$Ox2bQHezPywC.3ebMmrUruObKMIsHa8RkEfuLih7XhEDzMOOIblHy','Fernando','Dela Cruz','2017-05-07 00:00:00','2008-04-05 00:00:00','42 Gwernfadog Road','Morriston','Swansea','SA6 6RZ','United Kingdom',NULL,0,1,1,'01792 521983',NULL,'2010-06-22 00:00:00',1,NULL,NULL,NULL,NULL),
	(22,'lizeldelacruz94@yahoo.co.uk','$2y$13$dIDBYvD8Yh6E0IhuSOq7NepdRRk5x0Ui4poAyiaIItwslIL6pmvqy','Maria Lizel','Dela Cruz','2017-09-27 00:00:00','2010-04-03 00:00:00','42 Gwernfadog Road','Morriston','Swansea','SA6 6RZ','United Kingdom',NULL,0,1,1,'01792 521983',NULL,'2010-06-22 00:00:00',1,NULL,NULL,NULL,NULL),
	(23,'tituschamb@hotmail.com','$2y$13$IoakgV5mtUFY0Xx21yHF9el/VExhfRMHyRl9denfNZtiVPN9lLoPe','Tendayi','Chambati','2017-05-09 00:00:00','2013-04-06 00:00:00','15 Awel Deg','Birchgrove','Swansea','SA7 0HN','United Kingdom',NULL,0,0,1,'01792 796096',NULL,'2010-09-13 00:00:00',1,NULL,NULL,NULL,NULL),
	(24,'plaxiechambati@aol.com','$2y$13$13AzUjzqHC53FjRELdWr..82gP.HVoqv4Rvz9lAFUxXM4rh.iAmgi','Plaxedes','Chambati','1978-09-11 00:00:00','2013-04-06 00:00:00','15  Awel Deg','Birchgrove','Swansea','SA7 0HN','United Kingdom',NULL,0,0,1,'01792 796096',NULL,'2010-09-13 00:00:00',1,NULL,NULL,NULL,NULL),
	(25,'arnelzambrano@gmail.com','$2y$13$wduxbz.jW/YEFXBCqL55fOaZkP.I/7ITYi.UXfrbrCjFRojjE5Vuy','Arnel','Zambrano','1979-02-01 00:00:00','2014-04-05 00:00:00','43 Bryn Nedd','Cimla','Neath','SA11 1JJ','United Kingdom',NULL,0,1,1,'01639 645251',NULL,'2004-06-10 00:00:00',1,NULL,NULL,NULL,NULL),
	(26,'bridge_23ph@gmail.com','$2y$13$qFmkhYNI/pk8iqw0.jE4HuFSEsCwE/rxOlsk9x/ugoeEE8ABBJ4BK','Bridget','Zambrano','1976-07-23 00:00:00','2013-04-06 00:00:00','43 Bryn Nedd','Cimla','Neath','SA11 1JJ','United Kingdom',NULL,0,1,1,'07533 391728',NULL,'2004-06-10 00:00:00',1,NULL,NULL,NULL,NULL),
	(27,'ganda_wena@yahoo.com','$2y$13$757r0SLjddjQUJsdXdhaku0R3pvcNUlzT28vAFLCoZnUUlB1QxNi2','Rowena','Abuyan','1975-10-25 00:00:00','2008-04-05 00:00:00','90 Heol Maes y Gelynen','Morriston','Swansea','SA6 6JT','United Kingdom',NULL,0,1,3,'07957 938480',NULL,NULL,1,1,1,1,'Weng'),
	(28,'nildaamanpigos@yahoo.com','$2y$13$07jv1SE5P20CtttY7pE6iexECnr.p5dWA.gazpocyu7WFn.bInieO','Nilda','Pigos','1970-06-14 00:00:00','2008-04-05 00:00:00',NULL,'Bonymaen','Swansea',NULL,'United Kingdom',NULL,0,1,3,NULL,'07442 658515','1974-06-12 00:00:00',1,NULL,NULL,NULL,NULL),
	(29,NULL,'$2y$13$IDaZRmzEMsdy4NsizFLJTuD1mTPmpKg8Y8sOU2mlmt0H1jH0gdeSu','Lina','James','1948-06-14 00:00:00','2014-04-05 00:00:00','90 Heol Maes y Gelynen','Morriston','Swansea','SA6 6JT','United Kingdom',NULL,0,1,3,NULL,'07909 465153',NULL,1,NULL,NULL,NULL,NULL),
	(30,'nigelcora@hotmail.co.uk','$2y$13$7ZhCL1PgV/I5ygTKFHjq2eytY3M3KrRtlEMIVeuqZoi.f.xzeOLde','Maria Corazon','Crouch',NULL,'2011-04-02 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'07880 903205',NULL,1,NULL,NULL,NULL,NULL),
	(31,'arlyn1016@yahoo.com','$2y$13$yza.XSMh2V1MATF7uMKffO0oib3OC6hTlwy129ifGVg89x2qQztrW','Arlyn','Mansit','2010-09-21 00:00:00','2011-04-02 00:00:00','10 Angel Street',NULL,'Port Talbot',NULL,'United Kingdom',NULL,0,0,NULL,'01639 773228',NULL,NULL,1,NULL,NULL,NULL,NULL),
	(32,'mgjrsebite@yahoo.com','$2y$13$navmYfTLC2OMyfnkkXyRue2uulgD5eTsX6hHhStyW39KDlX9oMit2','Mary Grace','Sabio','1984-11-04 00:00:00','2011-04-02 00:00:00','25 Alfred Street',NULL,'Port Talbot','SA12  6UL','United Kingdom',NULL,0,0,NULL,'01639 769907',NULL,NULL,1,NULL,NULL,NULL,NULL),
	(33,'hazelfrancia@yahoo.com','$2y$13$Kw4OzJu2u/Po0bT5UHxdL.jd1BBrUxAoxU4yQH1tN/PuTuKrhF3Xm','Hazel Ann','Francia',NULL,'2014-05-19 00:00:00',NULL,NULL,'Port Talbot',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),
	(34,'triciaqp@yahoo.co.uk','$2y$13$efmiUY9dDKThT4M4csFGC.JsNJAdLcgvwG/NXlxBVH3.BW5mVyX7e','Tricia','Pinon','2017-04-11 00:00:00','2010-04-03 00:00:00','15 Clos Cadno','Morriston','Swansea',NULL,'United Kingdom',NULL,0,0,NULL,'01792 425930',NULL,NULL,1,NULL,NULL,NULL,NULL),
	(35,'marjz_050272@yahoo.com','$2y$13$al4zdLK3pH.CaXLwmLrTdOPqN.eMctzZ0wVwTUuvKLgNVR4t5RDba','Marjorie','Magsino','1974-02-05 00:00:00','2008-04-05 00:00:00','11 Heol y Deri','Morriston','Swansea','SA6 6JH','United Kingdom',NULL,0,0,NULL,'01792 541315',NULL,NULL,1,NULL,NULL,NULL,NULL),
	(36,'napoleonlorin@yahoo.co.uk','$2y$13$SzWLjM5gluHjvMmYHo5n4OLnCZGCRTjaqjiKYwdobNrQ5NXPUZFSy','Napoleon','Lorin','1971-12-26 00:00:00','2010-04-03 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),
	(37,'maringgay0418@yahoo.co.uk','$2y$13$DFLsADfKd96/xVWORQG4Ne5eOycPn8iJJScm7tXhaUCEt8zIEjnDu','Marilyn','Lorin','2074-05-29 00:00:00','2010-04-03 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),
	(38,'fradelossantos@gmail.com','$2y$13$OQPmLUuz9YGVfxdxowoeZO4PQklWNljIVX93wVAZXpYJfI3AqKh1m','Fe Riza Ann','Delos Santos','1976-06-11 00:00:00','2013-09-21 00:00:00','40 Roundwood','Llanedeyrn','Cardiff','CF23 9PF','United Kingdom',NULL,1,1,1,NULL,'07411 886375','2014-09-21 00:00:00',1,NULL,NULL,NULL,NULL),
	(39,'thebest_ofaikido94@yahoo.com','$2y$13$u2MUS1qyghcu5jhwg0v3lu6mNaGRWvlZdWqf.ELuxl7RcslFd0l/u','Larry','Cabrera','1964-07-08 00:00:00','2013-04-06 00:00:00','5 Blackmoor Place','Llanrumney','Cardiff','CF3 5TS','United Kingdom',NULL,0,0,1,'02922 210098',NULL,'2010-05-21 00:00:00',1,NULL,NULL,NULL,NULL),
	(40,'katryncabrera66@yahoo.co.uk','$2y$13$nM77mQ7FNt3asFWX/kbR0.pn6QHBNOMkMfOxwTpo193iGZ5dyqZ86','Kathryn','Cabrera','1966-07-08 00:00:00','2013-04-06 00:00:00','5 Blackmoor Place','Llanrumney','Cardiff','CF3 5TS','United Kingdom',NULL,1,1,1,'02922 210098',NULL,'2010-05-21 00:00:00',1,NULL,NULL,NULL,NULL),
	(41,NULL,'$2y$13$JIEox7.P16SVEYebI0jYMuvo7BxA0.9PmaZWTD/TS5zIpVQYEgoxS','Steve','Paget','2017-01-22 00:00:00','2014-04-05 00:00:00','21 Ashburton Avenue','Llanrumney','Cardiff','CF3 5PR','United Kingdom',NULL,0,1,1,NULL,NULL,'2015-04-25 00:00:00',1,NULL,NULL,NULL,NULL),
	(42,NULL,'$2y$13$rBkwwkajf3b3XIKvpJpyVesBY8XQsp56HRyWJfQl37N/iE58wB0aK','Conchita','Paget','1967-09-22 00:00:00','2014-04-05 00:00:00','21 Ashburton Avenue','Llanrumney','Cardiff','CF3 5PR','United Kingdom',NULL,0,1,1,NULL,NULL,'2015-04-25 00:00:00',1,NULL,NULL,NULL,NULL),
	(43,'acryd_10@yahoo.com','$2y$13$iUlNHQDAZDWd5YkR3ercpujYvg5B.J/aSkHK8RlTyY/nzV8PB8gt6','Nikki','Eclar','1976-02-10 00:00:00','2014-04-05 00:00:00','54 A Countisbury Avenue','Llanrumney','Cardiff','CF3 5SP','United Kingdom',NULL,1,1,3,NULL,'07957 746956',NULL,1,NULL,NULL,NULL,NULL),
	(44,NULL,'$2y$13$qpfhFQ6Vjq19JAq1CgPPd.ypLQj5qcnl6WgMCYxccN/uETVNTrsOW','Ariel','Tunay','1976-11-02 00:00:00','2013-04-06 00:00:00',NULL,'Morriston','Swansea',NULL,'United Kingdom',NULL,0,0,1,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),
	(45,NULL,'$2y$13$bJ1nHGG7WJ0BmUGmnHTwvePU5I9Nt2ZMD3TTyp7ezcgTvojkb7FTi','Marietta','Tunay','1976-11-02 00:00:00',NULL,NULL,'Morriston','Swansea',NULL,'United Kingdom',NULL,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),
	(46,NULL,'$2y$13$cUWSRO1DM5rdE1NpUf2VcuzXMeakw1giUX8yEAyQcF1.yAmsb7c9C','Myrna','Musngi','1953-12-29 00:00:00','2014-04-05 00:00:00',NULL,'Llanrumney','Cardiff',NULL,'United Kingdom',NULL,1,1,1,NULL,NULL,'2017-06-25 00:00:00',1,NULL,NULL,NULL,NULL),
	(47,NULL,'$2y$13$WQIfQ4U0mjBME/rLyc27retT82LQr0Zri2PWqP/O3pw.yJi4yJHNa','Ricardo','Musngi',NULL,NULL,NULL,'Llanrumney','Cardiff',NULL,'United Kingdom',NULL,0,1,1,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),
	(48,'renerey@yahoo.co.uk','$2y$13$YAcXYFDdXJr7.zjDnJQ0YepOLAFKZDsurDByO5uCC80RpIpCRnuXe','Rene Rey','Rivera','1973-08-31 00:00:00','2015-04-04 00:00:00','27 Glynne Jones Court',NULL,'Merthyr Tydfill','CF48 3DB','United Kingdom',NULL,0,1,1,NULL,'07749 592345',NULL,1,NULL,NULL,NULL,NULL),
	(49,'riveramaegraciel@yahoo.co.uk','$2y$13$pbiDNQqPFijtEW3mEBGfoORwUEh84uG7tLgUNzVaY9OXpnykgws7K','Mae Graciel','Rivera','2017-04-26 00:00:00','2015-04-04 00:00:00','27 Glynne Jones Court',NULL,'Merthyr Tydfill','CF48 3DB','United Kingdom',NULL,0,1,1,NULL,'07958 296530',NULL,1,NULL,NULL,NULL,NULL),
	(50,'totie_m@yahoo.com','$2y$13$roOfHagLJFTVuW9BcqidvOfV2RIHvWJj0FG508EkGb.Voodl.ueaq','Emilito','Mariano','1970-04-04 00:00:00','2015-04-04 00:00:00','5 Fernill Close',NULL,'Merthyr Tydfil','CF47 9DU','United Kingdom',NULL,0,1,1,NULL,'07446 960383','2010-12-30 00:00:00',1,1,1,2,NULL),
	(51,'mjmar_08@yahoo.com','$2y$13$wNBPyLR9e4/sXak6LtQvYelFc4PblPC0Udi/juYeLj/hMRVSkKDim','Mary Jane','Mariano','1973-08-02 00:00:00','2015-04-04 00:00:00','5 Fernill Close',NULL,'Merthyr Tydfil','CF47 9DU','United Kingdom',NULL,0,1,1,NULL,'07462 367476','2010-12-30 00:00:00',1,1,1,2,NULL),
	(52,'mgrace_uk2003@yahoo.com','$2y$13$lOGAgXybysIcGkJ/b4mLh.CpacZygFL3.gTrFAcK5G2OyqjD1dX6u','Mary Grace','Padua','1975-11-10 00:00:00','2015-04-04 00:00:00','36 Llwyn-Yr-Eos Grove','Bradley Gardens','Merthyr Tydfil','CF47 0GD','United Kingdom',NULL,0,1,1,NULL,NULL,NULL,1,1,1,2,NULL),
	(53,'celsojrpadua@yahoo.co.uk','$2y$13$mqe1plyH8psGKu6lkWOoS.oo9j3Tkas0fQo8u9ftqYB8qKoxZ97oS','Celso','Padua','1974-07-05 00:00:00','2016-04-02 00:00:00','36 Llwyn-Yr-Eos Grove','Bradley Gardens','Merthyr Tydfil','CF47 0GD','United Kingdom',NULL,0,1,1,NULL,'07810 488412',NULL,1,1,1,2,NULL),
	(54,'t.abastillas@yahoo.com','$2y$13$Q15BQa9H.jt7t5QSG//GyOKuRAVgZcAdsmTGXGl5baf26qELuXfMq','Teofilo, Jr','Abastillas',NULL,'2015-04-04 00:00:00','67 Forsythia Close',NULL,'Merthyr Tydfil','CF47 9DS','United Kingdom',NULL,0,1,1,NULL,'07412 665580',NULL,1,1,2,2,'Junie'),
	(55,NULL,'$2y$13$R4p1.WWzmKwsqlPMl.J2Web2DgsRb5pajQ5K7HLY0IbUA6xXViJWC','Ann Marie','Abastillas','1967-04-23 00:00:00','2015-04-04 00:00:00','67 Forsythia Close',NULL,'Merthyr Tydfil','CF47 9DS','United Kingdom',NULL,0,1,1,NULL,'07412 665580',NULL,1,1,1,2,'Ann'),
	(56,NULL,'$2y$13$wRltTUZZL2BcNVmhZ5rCduE1FC0MqeXeYXLqecNH/ed4sqUCi4wym','Jasmin','Gales',NULL,'2015-04-04 00:00:00','27 Forsythia Close',NULL,'Merthyr Tydfil','CF47 9DS','United Kingdom',NULL,0,1,1,NULL,'07584 095304',NULL,1,1,1,2,NULL),
	(57,'jclynfjrd@yahoo.co.uk','$2y$13$mnBpISDozCdN5xFEoMnciuSf4EgyEfD4MneqCM1RFxArriy45gBBG','Jocelyn','Fajardo','1968-04-25 00:00:00','2015-04-04 00:00:00','Flat 4 115','St. David’s Mansion','Cowbrigde',NULL,'United Kingdom',NULL,0,0,3,NULL,'07453 275541',NULL,1,1,1,2,'Jo'),
	(58,NULL,'$2y$13$A.4tMrcWmq55kGX79YqC8ONg1N1LqJSMQQzS5ZKkdR.GF9BnYRL3W','Jaime, II','Menor',NULL,'2014-04-05 00:00:00','13 Malmesmead Road','Llanrumney Road','Cardiff',NULL,'United Kingdom',NULL,0,0,1,NULL,'07886 197118',NULL,1,1,1,2,NULL),
	(59,'lady24792003@yahoo.com','$2y$13$4VmHNxwT3HRi7SzDpvow8.v22UqaSw.jk.pv9vMYR3GrWEtYrIg8q','Mary Nadia','Menor','1979-08-01 00:00:00','2014-04-05 00:00:00','13 Malmesmead Road','Llanrumney Road','Cardiff',NULL,'United Kingdom',NULL,0,0,1,NULL,NULL,NULL,1,1,1,2,'Nadia'),
	(60,'marlyn_briones@yahoo.co.uk','$2y$13$GSIuhM3853lmd1ckNc1OGOO8v0pZ.ZdeeNHcrltbg8PywWatPLd7a','Marlyn','Briones','1957-01-14 00:00:00','2016-04-02 00:00:00','8 King George V Drive','Heath','Cardiff','CF14 4FD','United Kingdom',NULL,0,1,3,NULL,'07921 481029',NULL,1,1,1,2,'Maz'),
	(61,'can_lyn_gwy@yahoo.com','$2y$13$E1LlHTPyJOvIPNIu2xWcQOQ8J75NM6T8oyxJbmlGVGELdi2.74Q06','Maria Noralyn','Molina','1968-02-01 00:00:00','2016-04-02 00:00:00','8 King George V Drive','Heath','Cardiff','CF14 4FD','United Kingdom',NULL,0,1,3,NULL,'07788 158560',NULL,1,1,1,2,'Lyn'),
	(62,NULL,'$2y$13$JLUFdctFABp1PLKbxolRH.Kn8X2KZt2Hlxgxdem1R8r.Oaxp62esu','Joan','Grabczewski','1970-05-29 00:00:00','2016-04-02 00:00:00','07 Daphne Road',NULL,'Neath',NULL,'United Kingdom',NULL,0,0,3,NULL,'07921 540407',NULL,1,1,3,2,NULL),
	(63,'sjmorg2002@yahoo.co.uk','$2y$13$Kl45.o/3kBjX7QDJHpaGQuE7/uKwbPklsSC1mMmyqz5iXTx9Egtke','Sarah','Morgan','1979-01-12 00:00:00','2016-04-02 00:00:00','15 St.Mary’s Road','Evesham','Worcestershire','WR11 4EG','United Kingdom',NULL,0,0,3,NULL,'07793 067726',NULL,1,1,2,2,NULL),
	(64,'ernieabenio@yahoo.com','$2y$13$MMhFglsAts0ots6ls85BgOzY7XlRd6FNNH5mZD0uhaAvSNUUZroJa','Ernie','Abenio','1974-03-20 00:00:00','2016-04-02 00:00:00','63 Parcmaen Street',NULL,'Carmarthen Dyfed',NULL,'United Kingdom',NULL,0,0,1,NULL,'01267 232392',NULL,1,1,3,2,NULL),
	(65,NULL,'$2y$13$d2CHTsdyLkllWrJTGRGGAORKT3qxC67jotY9PwUkYo12ub83ZTj0S','Romina','Abenio','1975-01-03 00:00:00','2016-04-02 00:00:00','63 Parcmaen Street',NULL,'Carmarthen Dyfed',NULL,'United Kingdom',NULL,0,0,1,NULL,'01267 232392',NULL,1,1,3,2,NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_groups`;

CREATE TABLE `user_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_953F224DA76ED395` (`user_id`),
  KEY `IDX_953F224DFE54D947` (`group_id`),
  CONSTRAINT `FK_953F224DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_953F224DFE54D947` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;

INSERT INTO `user_groups` (`user_id`, `group_id`)
VALUES
	(1,1),
	(1,8),
	(8,14),
	(12,5),
	(13,6),
	(14,7),
	(16,2),
	(17,2),
	(19,4),
	(20,4),
	(38,9),
	(42,4);

/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
