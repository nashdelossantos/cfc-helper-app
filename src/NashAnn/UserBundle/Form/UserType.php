<?php

namespace NashAnn\UserBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array(
                'attr'          => array(
                    'class'         => 'form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('lastname', TextType::class, array(
                'attr'          => array(
                    'class'     => 'form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('nickname', TextType::class, array(
                'required'      => false,
                'attr'          => array(
                    'class'     => 'form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('email', EmailType::class, array(
                'required'      => false,
                'attr'          => array(
                    'class'     => 'form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('birthday', DateTimeType::class, array(
                'widget'        => 'single_text',
                'format'        => 'MMMM d, yyyy',
                'required'      => false,
                'attr'          => array(
                    'class'     => 'datepicker form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('clpDate', DateTimeType::class, array(
                'widget'        => 'single_text',
                'format'        => 'MMMM d, yyyy',
                'label'         => 'CLP Date',
                'required'      => false,
                'attr'          => array(
                    'class'     => 'datepicker form-control',
                    'autocomplete'  => 'off',
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('weddingDate', DateTimeType::class, array(
                'widget'        => 'single_text',
                'format'        => 'MMMM d, yyyy',
                'required'      => false,
                'attr'          => array(
                    'class'     => 'datepicker form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('telephone', TextType::class, array(
                'required'      => false,
                'attr'          => array(
                    'class'     => 'form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('mobile', TextType::class, array(
                'required'      => false,
                'attr'          => array(
                    'class'     => 'form-control',
                    'autocomplete'  => 'off'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('address1', TextType::class, array(
                'required'      => false,
                'label'         => 'Address 1',
                'attr'          => array(
                    'class'     => 'form-control auto-geo'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('address2', TextType::class, array(
                'required'      => false,
                'label'         => 'Address 2',
                'attr'          => array(
                    'class'     => 'form-control auto-geo'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('city', TextType::class, array(
                'required'      => false,
                'attr'          => array(
                    'class'     => 'form-control auto-geo'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('postcode', TextType::class, array(
                'required'      => false,
                'attr'          => array(
                    'class'     => 'form-control auto-geo'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('country', TextType::class, array(
                'required'      => false,
                'attr'          => array(
                    'class'     => 'form-control auto-geo'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('avatarInput', FileType::class, array(
                'mapped'        => false,
                'label'         => 'Avatar',
                'required'      => false,
                'attr'          => array(
                    'class'     => 'hidden',
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))

            ->add('ministry', EntityType::class, array(
                'required'      => false,
                'class'         => 'NashAnnUserBundle:Ministry',
                'choice_label'  => 'name',
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
            ))
            ->add('roles', EntityType::class, array(
                'required'      => false,
                'multiple'      => true,
                'class'         => 'NashAnnUserBundle:Group',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->where('g.slug != :slug')
                        ->setParameter('slug', 'top_admin')
                        ->orderBy('g.name', 'ASC')
                    ;
                },
                'choice_label'  => 'name',
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('household', EntityType::class, array(
                'required'      => false,
                'multiple'      => false,
                'class'         => 'NashAnnUserBundle:Household',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC')
                    ;
                },
                'choice_label'  => 'name',
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('unit', EntityType::class, array(
                'required'      => false,
                'multiple'      => false,
                'class'         => 'NashAnnUserBundle:Unit',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC')
                    ;
                },
                'choice_label'  => 'name',
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('chapter', EntityType::class, array(
                'required'      => false,
                'multiple'      => false,
                'placeholder'   => false,
                'class'         => 'NashAnnUserBundle:Chapter',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC')
                    ;
                },
                'choice_label'  => 'name',
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('isTithing', CheckboxType::class, array(
                'required'      => false,
                'label'         => 'Regularly tithing',
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('isActive', CheckboxType::class, array(
                'required'      => false,
                'label'         => 'Member is active',
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'NashAnn\UserBundle\Entity\User',
            'allow_extra_fields'    => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'NashAnn_userbundle_user';
    }


}
