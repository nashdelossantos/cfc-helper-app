<?php

namespace NashAnn\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NashAnnUserBundle:Default:index.html.twig');
    }

    /**
     * [dashboardAction description]
     * @return [type] [description]
     */
    public function dashboardAction()
    {
    	$em = $this->getDoctrine()->getManager();

        $filter = array(
            'limit'     => 3,
            'orderBy'   => 'birthday',
            'orderDir'  => 'ASC'
        );

    	$upcomingBirtdayCelebrants = $em->getRepository('NashAnnUserBundle:User')->findUpcomingBirthdays($filter);

    	$groups = $em->getRepository('NashAnnUserBundle:Group')->findAll();

    	return $this->render('NashAnnUserBundle:Default:dashboard.html.twig', array(
    		'upcomingBirtdayCelebrants'		=> $upcomingBirtdayCelebrants
    	));
    }
}
