<?php

namespace NashAnn\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_PageSetup;

use NashAnn\UserBundle\Library\UserManager;
use NashAnn\UserBundle\Entity\User;
use NashAnn\UserBundle\Entity\Download;

class SettingController extends Controller
{
    /**
     * Display Setting index
     * @return [type] [description]
     */
    public function indexAction()
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('Access Denied!');
        }

        if (!$user->hasPermission('setting_can_read')) {
            throw new AccessDeniedHttpException('Permission Required!');
        }

        $em = $this->getDoctrine()->getManager();
        $start = new \DateTime('January 1, 2017');

        $ministry = $em->getRepository('NashAnnUserBundle:Ministry')->findOneByName('Youth For Christ');
        
        $count = $em->getRepository('NashAnnUserBundle:User')->findByFilter(array(
            'ministry'  => $ministry,
            'start'     => $start,
            'active'    => 'inactive',
        ));

        dump($count);
        die();

        return $this->render('NashAnnUserBundle:Front/Setting:index.html.twig', array());
    }

    /**
     * Export data to requested format
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function exportAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('Permission Required!');
        }
    
        if (!$data = $request->request->get('data')) {
            throw new NotFoundHttpException('Required Parameters Not Found!');
        }

        if (!$user->hasPermission('export_can_create')) {
            throw new AccessDeniedHttpException('Insufficient Permission!');
        }

        if (!$start = $data['start']) {
            throw new NotFoundHttpException('Start Date Required!');
        }

        if (!$end = $data['end']) {
            throw new NotFoundHttpException('End Date Required!');
        }
        $type       = $data['type'];
        $startYear  = date('Y', strtotime($start));
        $endYear    = date('Y', strtotime($end));

        $year       = $startYear;
        if ($startYear != $endYear) {
            $year = $startYear . '-' . $endYear;
        }

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('NashAnnUserBundle:User')->findBy(array('leader' => $user));

        $ministryArr = [];
        $cfcTotal   = 0;
        $sfcTotal   = 0;
        $yfcTotal   = 0;
        $kfcTotal   = 0;
        $holdTotal  = 0;
        $soldTotal  = 0;

        foreach ($users as $u) {
            if (!$ministry = $u->getMinistry()) {
                continue;
            }

            $status = 'inactive';
            if ($u->getIsActive()) {
                $status = 'active';
            }

            $ministryArr[strtoupper($ministry->getInitial())][$status][] = $u;
        }

        if (isset($ministryArr['CFC'])) {
            $cfcActiveTotal     = count($ministryArr['CFC']['active']);
            $cfcInactiveTotal   = count($ministryArr['CFC']['inactive']);
            $cfcTotal           = $cfcActiveTotal + $cfcInactiveTotal;
        }
        if (isset($ministryArr['SFC'])) {
            $sfcActiveTotal     = count($ministryArr['SFC']['active']);
            $sfcInactiveTotal   = count($ministryArr['SFC']['inactive']);
            $sfcTotal           = $sfcActiveTotal + $sfcInactiveTotal;
        }
        if (isset($ministryArr['YFC'])) {
            $yfcActiveTotal     = count($ministryArr['YFC']['active']);
            $yfcInactiveTotal   = count($ministryArr['YFC']['inactive']);
            $yfcTotal           = $yfcActiveTotal + $yfcInactiveTotal;
        }
        if (isset($ministryArr['KFC'])) {
            $kfcActiveTotal     = count($ministryArr['KFC']['active']);
            $kfcInactiveTotal   = count($ministryArr['KFC']['inactive']);
            $kfcTotal           = $kfcActiveTotal + $kfcInactiveTotal;
        }
        if (isset($ministryArr['HOLD'])) {
            $holdActiveTotal     = count($ministryArr['HOLD']['active']);
            $holdInactiveTotal   = count($ministryArr['HOLD']['inactive']);
            $holdTotal           = $holdActiveTotal + $holdInactiveTotal;
        }
        if (isset($ministryArr['SOLD'])) {
            $soldActiveTotal     = count($ministryArr['SOLD']['active']);
            $soldInactiveTotal   = count($ministryArr['SOLD']['inactive']);
            $soldTotal           = $soldActiveTotal + $soldInactiveTotal;
        }

        $fullname = $user->getFirstname() . ' ' . $user->getLastname();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator('CFC Helper App')
           ->setLastModifiedBy($fullname)
           ->setTitle("CFC International Missions Annual Evangelization Report")
           ->setSubject("CFC International Missions Annual Evangelization Report")
           ->setDescription("CFC International Missions Annual Evangelization Report")
           ->setKeywords("CFC International Missions Annual Evangelization Report")
           ->setCategory("IMAE");

        $phpExcelObject->setActiveSheetIndex(0)
           ->setCellValue('A1', 'CFC International Missions Annual Evangelization Report')
           ->setCellValue('C2', 'Year:')
           ->setCellValue('D2', $year)

           ->setCellValue('A3', 'Name of Country: United Kingdom')
           ->setCellValue('A4', '              CFC-UK South Region')
           
           ->setCellValue('C4', 'Date:')
           ->setCellValue('D4', date("M d, Y"))
           
           ->setCellValue('A6', 'PARTICULARS')
           ->setCellValue('B6', 'CFC')
           ->setCellValue('C6', 'SFC')
           ->setCellValue('D6', 'YFC')
           ->setCellValue('E6', 'KFC')
           ->setCellValue('F6', 'HOLD')
           ->setCellValue('G6', 'SOLD')
           ->setCellValue('H6', 'Total')
           
           ->setCellValue('A8', 'Members as of '. $start)
           ->setCellValue('A9', "ADD: New Members thru")
           ->setCellValue('A10', "       1) CLPs, Youth &  Kids camps")
           ->setCellValue('A11', "       2) Transferees from other areas,countries")
           ->setCellValue('A12', "       3) Transition/Cross over from other ministries")
           ->setCellValue('A13', "       4) Other Additions")
           ->setCellValue('A14', "Sub-Total")
           ->setCellValue('A15', "LESS: ")
           ->setCellValue('A16', "       1) Transferees to other areas,countries")
           ->setCellValue('A17', "       2) Transition/Cross over from other ministries")
           ->setCellValue('A18', "       3) Other Deductions - inactive, death, wrong input")
           ->setCellValue('A19', "Total Deductions")
           ->setCellValue('A20', "TOTAL MEMBERS")

          // Numbers
           ->setCellValue('B8', $cfcTotal)
           ->setCellValue('C8', $sfcTotal)
           ->setCellValue('D8', $yfcTotal)
           ->setCellValue('E8', $kfcTotal)
           ->setCellValue('F8', $holdTotal)
           ->setCellValue('G8', $soldTotal)
           ->setCellValue('H8', count($users))
        ;

        $phpExcelObject->getActiveSheet()->setTitle('Simple');
        $phpExcelObject->getActiveSheet()->getStyle('A1:A8')->getFont()->setBold(true);
        $phpExcelObject->getActiveSheet()->getStyle('B6:H6')->getFont()->setBold(true);
        $phpExcelObject->getActiveSheet()->getStyle("A1:H27")->getFont()->setSize(12);
        $phpExcelObject->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
        $phpExcelObject->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
        $phpExcelObject->getActiveSheet()->getStyle('A15')->getFont()->setBold(true);
        $phpExcelObject->getActiveSheet()->getStyle('A20')->getFont()->setBold(true);
        $phpExcelObject->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
        $phpExcelObject->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);

        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(45);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(11.5);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(11.5);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(11.5);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(11.5);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(11.5);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(11.5);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(11.5);

        $phpExcelObject->getActiveSheet()->getStyle('A6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcelObject->getActiveSheet()->getStyle('D2:D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $phpExcelObject->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        
            // Paper size / orientation
            $phpExcelObject->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

            // print area
            $phpExcelObject->getActiveSheet()->getPageSetup()->setPrintArea('A1:H20');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');

        /**
         * Save to file
         */
        $filepaths = $this->container->getParameter('file_paths');
        
        $fs         = new Filesystem();
        $path       = $filepaths['export'] . $user->getId() . '/';
        $filename   = $type . '-' . date('d-m-Y') . '.xls';


            try {
                $fs->mkdir($path, 0700);
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at ".$e->getPath();
            }

        $writer->save($path . $filename);

        // persist file
        $file = new Download();
        
        $file 
            ->setUser($user)
            ->setFile($filename)
        ;
        $em->persist($file);
        $em->flush();

        return new JsonResponse(array('file' => $file->getId()));
        
        //return $this->redirectToRoute('user_setting');

    }
}
