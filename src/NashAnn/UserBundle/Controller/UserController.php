<?php

namespace NashAnn\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use NashAnn\UserBundle\Entity\User;
use NashAnn\UserBundle\Entity\Group;
use NashAnn\PastoralBundle\Entity\Pastoral;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->redirectToRoute('security_login');
        }

        $filters         = [];
        $totals          = 0;
        $sub             = null;
        $usersNoMinistry = 0;

        $data = [
            'sub'   => null,
        ];

        if ($ministry = $request->get('ministry')) {
           $filters['ministry'] = $ministry;
           $data['sub']         = true;
        }

        $em = $this->getDoctrine()->getManager();

        if (count($filters) > 0) {
            $users = $em->getRepository('NashAnnUserBundle:User')->findByFilters($filters);
        } else {
            $users              = $em->getRepository('NashAnnUserBundle:User')->findAll();
            $totals             = $em->getRepository('NashAnnUserBundle:User')->findTotalByStatus();
            $usersNoMinistry    = $em->getRepository('NashAnnUserBundle:User')->findUsersWithNoMinistry();
        }

        return $this->render('NashAnnUserBundle:Front/User:index.html.twig', array(
            'data'              => $data,
            'users'             => $users,
            'totals'            => $totals,
            'usersNoMinistry'   => $usersNoMinistry,
        ));
    }

    /**
     * Creates a new user entity.
     *
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->redirectToRoute('security_login');
        }

        if (!$user->hasPermission('user_can_create')) {
            throw new AccessDeniedHttpException('Access Denied!');
        }

        $filters = [
            'order' => 'name',
            'sort'  => 'ASC',
            'admin' => false,
        ];
        $em = $this->getDoctrine()->getManager();
        $groups = $em->getRepository('NashAnnUserBundle:Group')->findGroups($filters);

        $newUser = new User();
        $newUser->setLeader($user);

        $form = $this->createForm('NashAnn\UserBundle\Form\UserType', $newUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($newUser);
            $em->flush($newUser);

            return $this->redirectToRoute('user_index', array('id' => $newUser->getId()));
        }

        return $this->render('NashAnnUserBundle:Front/User:form.html.twig', array(
            'userRoleIds'   => [],
            'user'          => $newUser,
            'groups'        => $groups,
            'form'          => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     */
    public function showAction($id = 0)
    {
        $authUser = $this->getUser();
        if (!$authUser instanceof User) {
            return $this->render('error/401.html.twig'); // unauthorized
        }

        $em = $this->getDoctrine()->getManager();

        if (!$user = $em->getRepository('NashAnnUserBundle:User')->findOneById($id)) {
            throw new NotFoundHttpException("User not found");
        }

        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function editAction(Request $request, $id = 0)
    {
        $authUser = $this->getUser();
        if (!$authUser instanceof User) {
            return $this->render('error/401.html.twig'); // unauthorized
        }

        // << Get file paths
        $param          = $this->getParameter('file_paths');
        $uploadPath     = $param['uploadPath'];
        $uploadDir      = $param['upload'];
        $avatarDir      = $param['avatar'];
        $webDir         = $param['web'];
        // >> Get file paths

        $em = $this->getDoctrine()->getManager();

        if (!$user = $em->getRepository('NashAnnUserBundle:User')->findOneById($id)) {
            throw new NotFoundHttpException('User not found!');
        }

        $filters = [
            'order' => 'name',
            'sort'  => 'ASC',
            'admin' => false,
        ];
        $groups = $em->getRepository('NashAnnUserBundle:Group')->findGroups($filters);

        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('NashAnn\UserBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            // << Get uploaded avatar
            $avatarFileBag = $request->files->get('NashAnn_userbundle_user');
            if ($avatar = $avatarFileBag['avatarInput']) {
                $fileName = md5(uniqid()) . '.' . $avatar->guessExtension();
                $user->setAvatar($fileName);

                $image = Image::make($avatar);
                $image->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save(
                    $uploadPath . $avatarDir . $this->getUser()->getId() . '/' . $fileName, 70
                );
            }
            // >> Get uploaded avatar
            
            if (!$user->getLeader()) {
                $user->setLeader($authUser);
                $em->persist($authUser);
            }

            $em->persist($user);
            $em->flush($user);

            return $this->redirectToRoute('user_index', array('id' => $user->getId()));
        }

        return $this->render('NashAnnUserBundle:Front/User:form.html.twig', array(
            'user'          => $user,
            'avatarDir'     => null, //$webDir . $uploadDir . $avatarDir . $this->getUser()->getId() . '/',
            'groups'        => $groups,
            'form'          => $editForm->createView(),
            'delete_form'   => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush($user);
        }

        return $this->redirectToRoute('member_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * User's list
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function userListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('error' => 'Unauthorised Access!'));
        }

        $user = $this->getUser();
        if (!$user instanceof User) {
            return new JsonResponse(array('error' => 'Unauthorised Access!'));
        }

        $listView = $this->renderView('NashAnnUserBundle:Front/Ajax:list.html.twig', array(
            'users'     => $user->getMembers(),
        ));

        return new JsonResponse($listView);
    }

    /**
     * Get details for single user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function userDetailAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('error' => 'Unauthorised Access!'));
        }

        $user = $this->getUser();
        if (!$user instanceof User) {
            return new JsonResponse(array('error' => 'Unauthorised Access!'));
        }

        if (!$id = $request->request->get('id')) {
            return new JsonResponse(array('error' => 'Invalid Member ID'));
        }

        if (!$member = $em->getRepository('NashAnnUserBundle:User')->findOneById($id)) {
            return new JsonResponse(array('error' => 'Member Not Found!'));
        }

        if (!$user->getMembers()->contains($member)) {
            return new JsonResponse(array('error' => 'Unauthorised Access!'));
        }

        return new JsonResponse(array('user' => array(
            'address1'      => $member->getAddress1(),
            'address2'      => $member->getAddress2(),
            'city'          => $member->getCity(),
            'postcode'      => $member->getPostcode(),
            'country'       => $member->getCountry(),
            'telephone'     => $member->getTelephone(),
        )));
    }
}
