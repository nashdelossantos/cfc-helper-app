<?php

namespace NashAnn\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use NashAnn\UserBundle\Entity\User;

class DownloadController extends Controller
{
	public function fileAction($id = 0)
	{
		$user = $this->getUser();
		if (!$user instanceof User) {
			throw new AccessDeniedHttpException('Access Denied!');
		}

		if (!$user->hasPermission('download_can_download')) {
			throw new AccessDeniedHttpException('Required Permission Not Met!');
		}

		if (!$id) {
			throw new NotFoundHttpException("Invalid Download ID");
		}

		$em = $this->getDoctrine()->getManager();

		if (!$file = $em->getRepository('NashAnnUserBundle:Download')->findOneById($id)) {
			throw new NotFoundHttpException('File Not Found!');
		}

		$filepaths = $this->container->getParameter('file_paths');
		$export    = $filepaths['export'];

		//$response = new BinaryFileResponse('export/1/imac-23-06-2017.xls');
		$response = new BinaryFileResponse($export . $user->getId() . '/' . $file->getFile());
		$response->headers->set('Content-Type', 'application/vnd.ms-office');
		$response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

		return $response;
	}
}
