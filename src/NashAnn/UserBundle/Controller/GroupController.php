<?php

namespace NashAnn\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use NashAnn\UserBundle\Entity\Group;

class GroupController extends Controller
{
	public function newAction(Request $request)
	{
		$groupName = $request->request->get('group');
		
		$em = $this->getDoctrine()->getManager();

		// Check if role name already exists or has something similar
		if (!$groupExists = $em->getRepository('NashAnnUserBundle:Group')->findOneByName($groupName)) {
			
			$group = new Group();
			$group->setName($groupName);
			$group->setSlug(str_replace(' ', '-', strtolower($groupName)));

			$em->persist($group);
			$em->flush();
			
			return new JsonResponse(array(
				'success' 	=> 'Group successfully added',
				'id'		=> $group->getId()
			));
		}

		return new JsonResponse(array(
			'exists'	=> $groupExists->getName()
		));

	}
}
