<?php

namespace NashAnn\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ministry
 *
 * @ORM\Table(name="ministry")
 * @ORM\Entity(repositoryClass="NashAnn\UserBundle\Repository\MinistryRepository")
 */
class Ministry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="NashAnn\UserBundle\Entity\User", mappedBy="group")
     */
    private $users;

    /**
     * @ORM\Column(name="initial", type="string", length=6, nullable=true)
     */
    private $initial;

    /**
     * @ORM\Column(name="name", type="string", length=60)
     */
    private $name;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Ministry
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set initial
     *
     * @param string $initial
     *
     * @return Ministry
     */
    public function setInitial($initial)
    {
        $this->initial = $initial;

        return $this;
    }

    /**
     * Get initial
     *
     * @return string
     */
    public function getInitial()
    {
        return $this->initial;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \NashAnn\UserBundle\Entity\User $user
     *
     * @return Ministry
     */
    public function addUser(\NashAnn\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \NashAnn\UserBundle\Entity\User $user
     */
    public function removeUser(\NashAnn\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
