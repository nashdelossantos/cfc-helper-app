<?php

namespace NashAnn\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\UserBundle\Model\User as BaseUser;

use NashAnn\UserBundle\Entity\Group;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @ORM\Entity(repositoryClass="NashAnn\UserBundle\Repository\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable, ParticipantInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="NashAnn\UserBundle\Entity\Group", inversedBy="users", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="user_groups")
     */
    private $groups;

    /**
     * @ORM\ManyToOne(targetEntity="NashAnn\UserBundle\Entity\Ministry")
     * @ORM\JoinColumn(name="ministry", referencedColumnName="id")
     */
    private $ministry;

    /**
     * @ORM\OneToMany(targetEntity="NashAnn\UserBundle\Entity\User", mappedBy="leader")
     */
    private $members;

    /**
     * @ORM\ManyToOne(targetEntity="NashAnn\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="leader", referencedColumnName="id")
     */
    private $leader;

    /**
     * @ORM\ManyToOne(targetEntity="NashAnn\UserBundle\Entity\Chapter")
     * @ORM\JoinColumn(name="chapter", referencedColumnName="id")
     */
    private $chapter;

    /**
     * @ORM\ManyToOne(targetEntity="NashAnn\UserBundle\Entity\Household")
     * @ORM\JoinColumn(name="household", referencedColumnName="id")
     */
    private $household;

    /**
     * @ORM\ManyToOne(targetEntity="NashAnn\UserBundle\Entity\Unit")
     * @ORM\JoinColumn(name="unit", referencedColumnName="id")
     */
    private $unit;

    /**
     * @ORM\OneToMany(targetEntity="NashAnn\TaxonomyBundle\Entity\Taxonomy", mappedBy="user")
     */
    private $taxonomies;

    /**
     * @ORM\OneToMany(targetEntity="NashAnn\ActivityBundle\Entity\Activity", mappedBy="user")
     */
    private $activities;

    /**
     * @ORM\Column(name="email", type="string", length=60, nullable=true, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(name="nickname", type="string", length=30, nullable=true)
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(name="telephone", type="string", length=30, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(name="mobile", type="string", length=30, nullable=true)
     */
    private $mobile;

    /**
     * @ORM\Column(name="wedding_date", type="datetime", nullable=true)
     */
    private $weddingDate;

    /**
     * @ORM\Column(name="clp_date", type="datetime", nullable=true)
     */
     private $clpDate;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(name="postcode", type="string", length=10, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(name="is_tithing", type="boolean")
     */
    private $isTithing = false;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive = true;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Get email
    * 
    * @return  
    */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
    * Set email
    * 
    * @return $this
    */
    public function setEmail($email)
    {
        $this->email = $email;
        
        return $this;
    }

    /**
    * Get 
    * @return  
    */
    public function getUsername()
    {
        return $this->email;
    }

    /**
    * Get password
    * @return  
    */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
    * Set password
    * @return $this
    */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
    * Get plainPassword
    * @return  
    */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
    
    /**
    * Set plainPassword
    * @return $this
    */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        // forces the object to look "dirty" to Doctrine. Avoids
        // Doctrine *not* saving this entity, if only plainPassword changes
        $this->password = null;

        return $this;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
    * Get birthday
    * @return  
    */
    public function getBirthday()
    {
        $birthday = $this->birthday;

        //April 12th 2017, 12:00 am
        return $birthday;
    }
    
    /**
    * Set birthday
    * @return $this
    */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
    * Get clpDate
    * @return  
    */
    public function getClpDate()
    {
        $clpDate = $this->clpDate;

        return $clpDate;
    }
    
    /**
    * Set clpDate
    * @return $this
    */
    public function setClpDate($clpDate)
    {
        $this->clpDate = $clpDate;
        return $this;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return User
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return User
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return User
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
    * Get avatar
    * @return  
    */
    public function getAvatar()
    {
        return $this->avatar;
    }
    
    /**
    * Set avatar
    * @return $this
    */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
    * Get isActive
    * @return  
    */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    /**
    * Set isActive
    * @return $this
    */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    public function getSalt()
    {
    }
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Methods to check if User is active or now
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    // serialize and unserialize must be updated - see below
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->isActive
        ));
    }
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive
        ) = unserialize($serialized);
    }

    /**
     * Set isTithing
     *
     * @param boolean $isTithing
     *
     * @return $this
     */
    public function setIsTithing($isTithing)
    {
        $this->isTithing = $isTithing;

        return $this;
    }

    /**
     * Get isTithing
     *
     * @return boolean
     */
    public function getIsTithing()
    {
        return $this->isTithing;
    }

        /**
    * Get roles
    * @return  
    */
    public function getRoles()
    {
        //$roles =  $this->groups;
        //return $roles;
        return array('ROLE_USER');
    }
    
    /**
    * Set roles
    * @return $this
    */
    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->taxonomies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add group
     *
     * @param \NashAnn\UserBundle\Entity\Group $group
     *
     * @return User
     */
    public function addGroup(\NashAnn\UserBundle\Entity\Group $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \NashAnn\UserBundle\Entity\Group $group
     */
    public function removeGroup(\NashAnn\UserBundle\Entity\Group $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set ministry
     *
     * @param \NashAnn\UserBundle\Entity\Ministry $ministry
     *
     * @return User
     */
    public function setMinistry(\NashAnn\UserBundle\Entity\Ministry $ministry = null)
    {
        $this->ministry = $ministry;

        return $this;
    }

    /**
     * Get ministry
     *
     * @return \NashAnn\UserBundle\Entity\Ministry
     */
    public function getMinistry()
    {
        return $this->ministry;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return User
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set weddingDate
     *
     * @param \DateTime $weddingDate
     *
     * @return User
     */
    public function setWeddingDate($weddingDate)
    {
        $this->weddingDate = $weddingDate;

        return $this;
    }

    /**
     * Get weddingDate
     *
     * @return \DateTime
     */
    public function getWeddingDate()
    {
        return $this->weddingDate;
    }

    /**
     * Add member
     *
     * @param \NashAnn\UserBundle\Entity\User $member
     *
     * @return User
     */
    public function addMember(\NashAnn\UserBundle\Entity\User $member)
    {
        $this->members[] = $member;

        return $this;
    }

    /**
     * Remove member
     *
     * @param \NashAnn\UserBundle\Entity\User $member
     */
    public function removeMember(\NashAnn\UserBundle\Entity\User $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Set leader
     *
     * @param \NashAnn\UserBundle\Entity\User $leader
     *
     * @return User
     */
    public function setLeader(\NashAnn\UserBundle\Entity\User $leader = null)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Get leader
     *
     * @return \NashAnn\UserBundle\Entity\User
     */
    public function getLeader()
    {
        return $this->leader;
    }

    /**
     * Set chapter
     *
     * @param \NashAnn\UserBundle\Entity\Chapter $chapter
     *
     * @return User
     */
    public function setChapter(\NashAnn\UserBundle\Entity\Chapter $chapter = null)
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * Get chapter
     *
     * @return \NashAnn\UserBundle\Entity\Chapter
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Set household
     *
     * @param \NashAnn\UserBundle\Entity\Household $household
     *
     * @return User
     */
    public function setHousehold(\NashAnn\UserBundle\Entity\Household $household = null)
    {
        $this->household = $household;

        return $this;
    }

    /**
     * Get household
     *
     * @return \NashAnn\UserBundle\Entity\Household
     */
    public function getHousehold()
    {
        return $this->household;
    }

    /**
     * Set unit
     *
     * @param \NashAnn\UserBundle\Entity\Unit $unit
     *
     * @return User
     */
    public function setUnit(\NashAnn\UserBundle\Entity\Unit $unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return \NashAnn\UserBundle\Entity\Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Check if user has permission
     * @param  [type]  $permission [description]
     * @return boolean             [description]
     */
    public function hasPermission($permission = null)
    {
        if ($permission) {
            $groups = $this->getGroups();

            foreach ($groups as $group) {
               if (in_array($permission, explode(',', $group->getPermissions()))) {
                    return true;
               }
            }
        }
        return false;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     *
     * @return User
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Add taxonomy
     *
     * @param \NashAnn\TaxonomyBundle\Entity\Taxonomy $taxonomy
     *
     * @return User
     */
    public function addTaxonomy(\NashAnn\TaxonomyBundle\Entity\Taxonomy $taxonomy)
    {
        $this->taxonomies[] = $taxonomy;

        return $this;
    }

    /**
     * Remove taxonomy
     *
     * @param \NashAnn\TaxonomyBundle\Entity\Taxonomy $taxonomy
     */
    public function removeTaxonomy(\NashAnn\TaxonomyBundle\Entity\Taxonomy $taxonomy)
    {
        $this->taxonomies->removeElement($taxonomy);
    }

    /**
     * Get taxonomies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxonomies()
    {
        return $this->taxonomies;
    }

    /**
     * Add activity
     *
     * @param \NashAnn\ActivityBundle\Entitiy\Activity $activity
     *
     * @return User
     */
    public function addActivity(\NashAnn\ActivityBundle\Entity\Activity $activity)
    {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \NashAnn\ActivityBundle\Entitiy\Activity $activity
     */
    public function removeActivity(\NashAnn\ActivityBundle\Entity\Activity $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }
}
