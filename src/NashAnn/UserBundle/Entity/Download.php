<?php

namespace NashAnn\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Download
 *
 * @ORM\Table(name="download")
 * @ORM\Entity(repositoryClass="NashAnn\UserBundle\Repository\DownloadRepository")
 */
class Download
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="times_downloaded", type="integer", nullable=true)
     */
    private $timesDownloaded;

    public function __construct()
    {
        $this->created = new \Datetime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Download
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Download
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set timesDownloaded
     *
     * @param integer $timesDownloaded
     *
     * @return Download
     */
    public function setTimesDownloaded($timesDownloaded)
    {
        $this->timesDownloaded = $timesDownloaded;

        return $this;
    }

    /**
     * Get timesDownloaded
     *
     * @return integer
     */
    public function getTimesDownloaded()
    {
        return $this->timesDownloaded;
    }

    /**
     * Set user
     *
     * @param \NashAnn\UserBundle\Entity\User $user
     *
     * @return Download
     */
    public function setUser(\NashAnn\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \NashAnn\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
