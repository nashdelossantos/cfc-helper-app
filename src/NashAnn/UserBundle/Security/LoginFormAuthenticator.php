<?php

namespace NashAnn\UserBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;


use NashAnn\UserBundle\Form\LoginForm;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{

    private $formFactory;
    private $router;
    private $em;

    /**
     * Initialise injectors
     * @param FormFactoryInterface $formFactory [description]
     * @param EntityManager        $em          [description]
     * @param RouterInterface      $router      [description]
     */
    public function __construct(FormFactoryInterface $formFactory, EntityManager $em, RouterInterface $router, UserPasswordEncoder $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->formFactory  = $formFactory;
        $this->router       = $router;
        $this->em           = $em;
    }
    

    /**
     * Get credentials 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getCredentials(Request $request)
    {
        $isLoginSubmit = $request->getPathInfo() == '/member/login' && $request->isMethod('POST');
        if (!$isLoginSubmit) {
            // skip authentication
            return;
        }

        $form = $this->formFactory->create(LoginForm::class);
        $form->handleRequest($request);

        $data = $form->getData();
        $request->getSession()
                ->set(Security::LAST_USERNAME,
                    $data['_username']);

        return $data;
    }

    /**
     * Get user credentials
     * @param  [type]                $credentials  [description]
     * @param  UserProviderInterface $userProvider [description]
     * @return [type]                              [description]
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['_username'];

        return $this->em->getRepository('NashAnnUserBundle:User')->findOneBy(['email' => $username]);
        //return $this->em->getRepository('NashAnnUserBundle:User')->findOneUsernameOrEmail($username);
    }

    /**
     * Check user credentials
     * @param  [type]        $credentials [description]
     * @param  UserInterface $user        [description]
     * @return [type]                     [description]
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['_password'];
        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            return true;
        }
        
        return false;
    }

    /**
     * Get login url
     * @return [type] [description]
     */
    protected function getLoginUrl()
    {
        return $this->router->generate('security_login');
    }

    /**
     * Default success url reirection
     * @return [type] [description]
     */
    protected function getDefaultSuccessRedirectUrl()
    {
        return $this->router->generate('nash_ann_front_homepage');
    }
}
