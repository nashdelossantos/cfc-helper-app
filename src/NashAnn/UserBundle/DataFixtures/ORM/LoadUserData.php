<?php

namespace NashAnn\UseholdBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NashAnn\UserBundle\Entity\User;
use NashAnn\UserBundle\Entity\Group;
use NashAnn\UserBundle\Entity\Ministry;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // << Load Users

        $em = $this->container->get('doctrine');
        $user = [
            'firstname' => 'Nash',
            'lastname'  => 'delos Santos',
            'postcode'  => 'CF23 9PF',
            'address1'  => '40 Roundwood',
            'address2'  => 'Llanedeyr',
            'city'      => 'Cardiff',
            'country'   => 'United Kingdom',
            'avatar'    => null,
            'birthday'  => null,
            'email'     => 'nashsaint@gmail.com',
            'password'  => '$2a$06$VfewEQJKoPitDTYXmeVbLuHDG2KvzgGl83Sa9nUKmeOJFOD3/XyYa',
            'roles'     => ['TOP_ADMIN'],
            'is_active' => 1,
            'clp_date'  => null,
        ];

        $newUser = new User();
        $newUser->setFirstName($user['firstname']);
        $newUser->setLastName($user['lastname']);
        $newUser->setPostcode($user['postcode']);
        $newUser->setAddress1($user['address1']);
        $newUser->setAddress2($user['address2']);
        $newUser->setCity($user['city']);
        $newUser->setCountry($user['country']);
        $newUser->setAvatar($user['avatar']);
        $newUser->setBirthday($user['birthday']);
        $newUser->setEmail($user['email']);
        $newUser->setPassword($user['password']);

<<<<<<< Updated upstream
=======
        $users = [
            [
                'firstname'     => 'Nash',
                'lastname'      => 'delos Santos',
                'postcode'      => 'CF23 9PF',
                'address1'      => '40 Roundwood',
                'address2'      => 'Llanedeyr',
                'birthday'      => 'June 12, 1975',
                'mobile'        => '07894 790939',
                'wedding_date'  => 'January 24, 2004', 
                'city'          => 'Cardiff',
                'country'       => 'United Kingdom',
                'avatar'        => null,
                'birthday'      => null,
                'email'         => 'nashsaint@gmail.com',
                'password'      => 'test',
                'roles'         => ['TOP_ADMIN'],
                'is_tithing'    => true,
                'is_active'     => 1,
                'clp_date'      => null,
            ]
        ];

        foreach ($users as $key => $user) {
            $newUser = new User();
            $newUser->setFirstName($user['firstname'])
                ->setLastName($user['lastname'])
                ->setPostcode($user['postcode'])
                ->setAddress1($user['address1'])
                ->setAddress2($user['address2'])
                ->setMobile($user['mobile'])
                ->setWeddingDate(new \Datetime($user['wedding_date']))
                ->setCity($user['city'])
                ->setCountry($user['country'])
                ->setAvatar($user['avatar'])
                ->setBirthday(new \Datetime($user['birthday']))
                ->setEmail($user['email'])
                ->setIsTithing($user['is_tithing'])
                ->setPassword('$2y$10$i.iFiov19Fp.BZMsuXQGfu3uGDQrhZzrBDZ.zFIXfw276YdmlaoFi') //nashann2004
            ;
            
            //$encoder = $this->container->get('security.password_encoder');
            //$encoder = $this->container->get('security.authentication.guard_handler');
            //$encoder->authenticateUserAndHandleSuccess(
            //    $newUser,
            //    $this->container->get('request'),
            //    $this->container->get('app.security.login_form_authenticator'),
            //    'main'
            //);
            //$password = $encoder->encodePassword($newUser, $user['password']);
            //$newUser->setPassword($password);
        }
        // >> Load Users
        
>>>>>>> Stashed changes

        // << Load Roles
        $roles = [
            ['initial' => 'TA',     'name'   => 'Top Admin',           'slug' => 'top_admin',          'role' => 'ROLE_TOP_ADMIN',              'permissions' =>
                "
                    user_can_create, user_can_read, user_can_update, user_can_delete
                "
            ],
            ['initial' => 'KFCC',   'name'   => 'KFC Coordinator',     'slug' => 'kfc_coordinator',    'role' => 'ROLE_KFC_COORDINATOR'],
            ['initial' => 'YFCC',   'name'   => 'YFC Coordinator',     'slug' => 'yfc_coordinator',    'role' => 'ROLE_YFC_COORDINATOR'],
            ['initial' => 'MMC',    'name'   => 'Music Ministry',      'slug' => 'music_ministry',     'role' => 'ROLE_MUSIC_MINISTRY'],

            ['initial' => 'HH',     'name'   => 'Household Head',      'slug' => 'household_head',     'role' => 'ROLE_HOUSEHOLD_HEAD'],
            ['initial' => 'HL',     'name'   => 'Household Leader',    'slug' => 'household_leader',   'role' => 'ROLE_HOUSEHOLD_LEADER'],
            ['initial' => 'UH',     'name'   => 'Unit Head',           'slug' => 'unit_head',          'role' => 'ROLE_UNIT_HEAD'],
            ['initial' => 'UL',     'name'   => 'Unit Leader',         'slug' => 'unit_leader',        'role' => 'ROLE_UNIT_LEADER'],
            ['initial' => 'CH',     'name'   => 'Chapter Head',        'slug' => 'chapter_head',       'role' => 'ROLE_CHAPTER_HEAD'],
            ['initial' => 'CL',     'name'   => 'Chapter Leader',      'slug' => 'chapter_leader',     'role' => 'ROLE_CHAPTER_LEADER'],
            ['initial' => 'RH',     'name'   => 'Region Head',         'slug' => 'region_head',        'role' => 'ROLE_REGION_HEAD'],
            ['initial' => 'RL',     'name'   => 'Region Leader',       'slug' => 'region_leader',      'role' => 'ROLE_REGION_LEADER'],
            ['initial' => 'HC',     'name'   => 'Handmaid Coordinator','slug' => 'handmaid_coordinator','role' => 'ROLE_HANDMAID_COORDINATOR'],
        ];

        $admin = null;
        foreach ($roles as $role) {
            if ($role['slug'] == 'top_admin') {
<<<<<<< Updated upstream
                $admin = new Group();
                $admin->setInitial($role['initial']);
                $admin->setName($role['name']);
                $admin->setSlug($role['slug']);
                $admin->setRole($role['role']);
                $admin->setPermissions($role['permissions']);

                $manager->persist($admin);
                continue;
=======
                $adminRole = new Group();
                $adminRole->setInitial($role['initial']);
                $adminRole->setName($role['name']);
                $adminRole->setSlug($role['slug']);
                $adminRole->setRole($role['role']);
                $adminRole->setPermissions($role['permissions']);
                $manager->persist($adminRole);
            } else {
                $group = new Group();
                $group->setInitial($role['initial']);
                $group->setName($role['name']);
                $group->setSlug($role['slug']);
                $group->setRole($role['role']);
                $manager->persist($group);
>>>>>>> Stashed changes
            }

            $group = new Group();
            $group->setRole($role['role']);
            $group->setInitial($role['initial']);
            $group->setName($role['name']);
            $group->setSlug($role['slug']);

            $manager->persist($group);
        }
        // >> Load Roles

        // << Load Ministry
        $ministries = [
            'cfc'   => 'Couples For Christ',
            'sold'  => 'Servants Of The Lord',
            'hold'  => 'Handmaids Of The Lord',
            'sfc'   => 'Singles For Christ',
            'yfc'   => 'Youth For Christ',
            'kfc'   => 'Kids For Christ',
            'mn'    => 'Music Ministry'
        ];

        foreach ($ministries as $key => $value) {
<<<<<<< Updated upstream

            $ministry = new Ministry();
            $ministry
                ->setName($value)
                ->setInitial(ucwords($key))
            ;

            $manager->persist($ministry);

=======
            $ministry = new Ministry();
            $ministry 
                ->setName($value)
                ->setInitial(ucwords($key))
            ;
            
            $manager->persist($ministry);

            if ($key = 'cfc') {
                $newUser->setMinistry($ministry);
            }
>>>>>>> Stashed changes
        }
        // >> Load Ministry

        // Add a top admin is there isn't one
<<<<<<< Updated upstream
        $newUser->addGroup($admin);
        $manager->persist($newUser);

=======
        $newUser->addGroup($adminRole);
        $manager->persist($newUser);
        
>>>>>>> Stashed changes
        $manager->flush();

    }
}