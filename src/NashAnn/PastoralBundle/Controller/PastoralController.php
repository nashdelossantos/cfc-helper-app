<?php

namespace NashAnn\PastoralBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use NashAnn\PastoralBundle\Entity\Pastoral;
use NashAnn\PastoralBundle\Form\PastoralType;
use NashAnn\UserBundle\Entity\User;

/**
 * Pastoral controller.
 *
 */
class PastoralController extends Controller
{
    /**
     * Lists all pastoral entities.
     *
     */
    public function indexAction()
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->redirectToRoute('security_login');
        }

        $em = $this->getDoctrine()->getManager();

        $pastoral = $em->getRepository('NashAnnPastoralBundle:Pastoral')->findAll();

        return $this->render('NashAnnPastoralBundle:Front:index.html.twig', array(
            'pastoral' => $pastoral,
        ));
    }

    /**
     * Get pastoral for category
     *
     * @param  [type] $category [description]
     * @return [type]           [description]
     */
    public function categoryAction($category = null)
    {
        if (!$category) {
            throw new NotFoundHttpException("Pastoral category Invalid");
        }

        $em = $this->getDoctrine()->getManager();

        if (!$category = $em->getRepository('NashAnnTaxonomyBundle:Taxonomy')->findOneBySlug($category)) {
            throw new NotFoundHttpException("Category ".$category." Not Found!");
        }

        $pastoral  = $em->getRepository('NashAnnPastoralBundle:Pastoral')->findOneByCategory($category);

        return $this->render('NashAnnPastoralBundle:Front:index.html.twig', array(
            'pastoral' => $pastoral,
        ));
    }

    /**
     * Creates a new pastoral entity.
     *
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();

        if (!$user instanceof User || !$user->hasPermission('pastoral_can_create')) {
            return $this->redirectToRoute('security_login');
        }

        $pastoral = new Pastoral();

        $em = $this->getDoctrine()->getManager();

        $category = null;
        if ($newCategory = $request->get('category')) {
            $category = $em->getRepository('NashAnnTaxonomyBundle:Taxonomy')->findOneBySlug($newCategory);
        }
<<<<<<< Updated upstream
        $form = $this->createForm(PastoralType::class, $pastoral, array('by_reference' => $user));

=======
        $form = $this->createForm('NashAnn\PastoralBundle\Form\PastoralType', $pastoral);
        
>>>>>>> Stashed changes
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($pastoral);
            $em->flush();

            return $this->redirectToRoute('pastoral_index');
        }

        return $this->render('NashAnnPastoralBundle:Front/Pastoral:form.html.twig', array(
            'pastoral' => $pastoral,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pastoral entity.
     *
     */
    public function showAction(Pastoral $pastoral)
    {
        $deleteForm = $this->createDeleteForm($pastoral);

        return $this->render('pastoral/show.html.twig', array(
            'pastoral' => $pastoral,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pastoral entity.
     *
     */
    public function editAction(Request $request, Pastoral $pastoral)
    {
        $form = $this->createForm('NashAnn\PastoralBundle\Form\PastoralType', $pastoral);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pastoral_index');
        }

        return $this->render('NashAnnPastoralBundle:Front\Pastoral:form.html.twig', array(
            'pastoral'      => $pastoral,
            'form'          => $form->createView(),
        ));
    }

    /**
     * Deletes a pastoral entity.
     *
     */
    public function deleteAction(Request $request, Pastoral $pastoral)
    {
        $form = $this->createDeleteForm($pastoral);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pastoral);
            $em->flush();
        }

        return $this->redirectToRoute('pastoral_index');
    }

    /**
     * Creates a form to delete a pastoral entity.
     *
     * @param Pastoral $pastoral The pastoral entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pastoral $pastoral)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pastoral_delete', array('id' => $pastoral->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
