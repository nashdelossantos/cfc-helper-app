<?php

namespace NashAnn\PastoralBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use NashAnn\TaxonomyBundle\Entity\Taxonomy;

class PastoralType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categories', EntityType::class, array(
                'class'         => 'NashAnnTaxonomyBundle:Taxonomy',
                'choice_label'  => 'name',
                'choice_attr'   => function($val, $key, $index) {
                    return ['class' => $index];
                },
                'query_builder' => function(EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('t')
                        ->where('t.type = :pastoral')
                        ->setParameter('pastoral', Taxonomy::TYPE_PASTORAL)
                        ->andWhere("t.user = :user OR t.user is NULL")
                        ->setParameter('user', $options['by_reference'])
                        ->orderBy('t.name', 'ASC')
                    ;
                },
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
                'multiple'      => true
            ))
            ->add('title', TextType::class, array(
                'label'         => 'Title',
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
                'required'      => true
            ))
            ->add('description', TextareaType::class, array(
                'required'      => false,
                'label'         => 'Description',
                'attr'          => array(
                    'class'     => 'form-control'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label',
                    'rows'      => 10
                )
            ))
            ->add('venue', TextType::class, array(
                'required'      => false,
                'label'         => 'Venue',
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('date', DateTimeType::class, array(
                'widget'        => 'single_text',
                'attr'          => array(
                    'class'     => 'form-control',
                    'autocomplete'  => 'off',
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NashAnn\PastoralBundle\Entity\Pastoral'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nashann_pastoralbundle_pastoral';
    }


}
