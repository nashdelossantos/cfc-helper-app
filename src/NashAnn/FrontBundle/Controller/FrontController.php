<?php

namespace NashAnn\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontController extends Controller
{
    public function indexAction()
    {
        return $this->render('NashAnnFrontBundle:Front/Page:index.html.twig');
    }
}
