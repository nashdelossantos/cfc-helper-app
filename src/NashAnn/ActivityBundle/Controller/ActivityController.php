<?php

namespace NashAnn\ActivityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use NashAnn\UserBundle\Entity\User;
use NashAnn\ActivityBundle\Entity\Activity;
use NashAnn\ActivityBundle\Form\ActivityType;

class ActivityController extends Controller
{
    public function indexAction()
    {
    	$user = $this->getUser();
    	if (!$user instanceof User) {
    		$request->getSession()
    			->getFlashBag()
    			->add('error', 'Login required to view Activity list!')
    		;
    		return $this->redirectToRoute('security_login');
    	}

    	if (!$user->hasPermission('activity_can_read')) {
    		$request->getSession()
    			->getFlashBag()
    			->add('error', 'Access Denied!')
    		;

    		return $this->redirectToRoute('activity_index');
    	}

    	$em = $this->getDoctrine()->getManager();
    	$activities = $em->getRepository('NashAnnActivityBundle:Activity')->findBy(
    		array(
    			'user'	=> $user
    		),
    		array(
    			'date' 	=> 'ASC'
    		)
    	);

        return $this->render('NashAnnActivityBundle:Activity:index.html.twig', array(
        	'activities' => $activities,
        ));
    }

    /**
     * New Activity
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function newAction(Request $request)
    {
    	$user = $this->getUser();
    	if (!$user instanceof User) {
    		$request->getSession()
    			->getFlashBag()
    			->add('error', 'Login required to create an Activity!')
    		;
    		return $this->redirectToRoute('security_login');
    	}

    	if (!$user->hasPermission('activity_can_create')) {
    		$request->getSession()
    			->getFlashBag()
    			->add('error', 'Access Denied!')
    		;

    		return $this->redirectToRoute('activity_index');
    	}

    	$activity = new Activity();

    	$form = $this->createForm(ActivityType::class, $activity);
    	$form->handleRequest($request);

    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();

    		$activity->setUser($user);

    		$em->persist($activity);
    		$user->addActivity($activity);

    		$em->flush();

    		return $this->redirectToRoute('activity_index');
    	}

    	return $this->render('NashAnnActivityBundle:Activity:form.html.twig', array(
    		'form'	=> $form->createView(),
    	));
    }
}
