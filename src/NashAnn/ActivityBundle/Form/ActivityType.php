<?php

namespace NashAnn\ActivityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class ActivityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label'         => 'Name',
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
            ))
            ->add('date', DateTimeType::class, array(
                'widget'        => 'single_text',
                'attr'          => array(
                    'class'     => 'form-control',
                    'autocomplete'  => 'off',
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('venue', TextType::class, array(
                'label'         => 'Venue',
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NashAnn\ActivityBundle\Entity\Activity'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nashann_activitybundle_activity';
    }


}
