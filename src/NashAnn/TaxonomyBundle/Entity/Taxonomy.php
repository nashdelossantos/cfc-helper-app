<?php

namespace NashAnn\TaxonomyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Taxonomy
 *
 * @ORM\Table(name="taxonomy")
 * @ORM\Entity(repositoryClass="NashAnn\TaxonomyBundle\Repository\TaxonomyRepository")
 */
class Taxonomy
{
    const TYPE_PASTORAL           = 1;

    public function getTypeNames()
    {
        return [
            self::TYPE_PASTORAL   => 'Pastoral',
        ];
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
<<<<<<< Updated upstream
     * @ORM\ManyToOne(targetEntity="NashAnn\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;
=======
     * @ORM\Column(name="type", type="string", length=60, nullable=true)
     */
    private $type;
>>>>>>> Stashed changes

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Taxonomy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param \NashAnn\UserBundle\Entity\User $user
     *
     * @return Taxonomy
     */
    public function setUser(\NashAnn\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \NashAnn\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
<<<<<<< Updated upstream
=======

    /**
    * Get type
    * 
    * @return  
    */
    public function getType()
    {
        return $this->type;
    }
    
    /**
    * Set type
    * 
    * @return $this
    */
    public function setType($type)
    {
        $this->type = $type;
        
        return $this;
    }
>>>>>>> Stashed changes
}
