<?php

namespace NashAnn\TaxonomyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NashAnnTaxonomyBundle:Default:index.html.twig');
    }
}
